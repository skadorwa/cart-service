Chapter 4 - Step by Step 
====================================

oc new-build skadorwa/openshiftjenkins~https://github.com/siamaksade/jenkins-blueocean.git --name=jenkins-blueocean

oc new-app jenkins-ephemeral \
    -p NAMESPACE=citest \
    -p JENKINS_IMAGE_STREAM_TAG=jenkins-blueocean:latest \
    -p MEMORY_LIMIT=2Gi
	
oc create -f https://bitbucket.org/skadorwa/public_blue/raw/d0da76536793234c143c686468cb2dc612d9ccd7/jboss-image-streams.json
 
oc process -f https://bitbucket.org/skadorwa/public_blue/raw/d0da76536793234c143c686468cb2dc612d9ccd7/cart-template.yaml  -v IMAGE_STREAM_NAMESPACE=citest | oc create -f -
 
oc create -f https://bitbucket.org/skadorwa/cart-service/raw/1e8236fade8b6508a5303ac590a58c911505c22e/openshift/cart-pipeline.yaml

oc delete -f https://bitbucket.org/skadorwa/cart-service/raw/1e8236fade8b6508a5303ac590a58c911505c22e/openshift/cart-pipeline.yaml


### Jenkins Cloud on Kubernetes PODS ###

* Name: 

### Jenkins Cloud on Docker ###

* Name: cloudname
* Docker URL: tcp://host:4243
* Docker API Version: 1.29

#### Docker Templates ####

* Docker image: imagename
* Remote Filling System Root: /var/jenkins_home
* Labels: cloud label

##### Advanced #####

* Volumes: /data/jenkins_nfs:/var/jenkins_home (advanced tab - volume mapping)
* Ports Binding: 8081:8081

